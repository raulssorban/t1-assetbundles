﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    public int Version;
    public static LoadAssetBundles BundleLoader = new LoadAssetBundles ();

    public void Start ()
    {
        StartCoroutine ( Init () );
    }

    public IEnumerator Init ()
    {
        //
        // Clear cache
        //
        Caching.ClearCache ();

        //
        // Load bundles
        //
        {
            var mainAssetBundle = AssetBundle.LoadFromFile ( LoadAssetBundles.BundlesPath + "/Bundles" );
            var mainAssetBundleManifest = mainAssetBundle.LoadAllAssets<AssetBundleManifest> () [ 0 ];
            var assetBundleNames = mainAssetBundleManifest.GetAllAssetBundlesWithVariant ();

            foreach ( var assetBundle in assetBundleNames )
            {
                var fileName = LoadAssetBundles.BundlesPath + "/" + assetBundle;
                yield return StartCoroutine ( BundleLoader.LoadBundle ( assetBundle, fileName, Version, false ) );
                Debug.Log ( fileName );
            }
        }

        //
        // Warming up assets
        //
        {
            foreach ( var assetBundle in BundleLoader.Bundles )
            {
                if ( assetBundle.Value != null )
                {
                    if ( !assetBundle.Value.isStreamedSceneAssetBundle )
                    {
                        foreach ( var assetName in assetBundle.Value.GetAllAssetNames () )
                        {
                            BundleLoader.Files.Add ( assetName, assetBundle.Value );
                            assetBundle.Value.LoadAssetWithSubAssets ( assetName );
                            Debug.Log ( assetName );

                            yield return null;
                        }
                    }
                }

                yield return null;
            }

            Debug.Log ( BundleLoader.Files.Count + " files" );
        }

        //
        // Spawn a bundled object
        //
        {
            var cubeObj = BundleLoader.LoadAsset<GameObject> ( "assets/bundled/prefabs/cube.prefab", false );
            var cubeObjInstance = Instantiate ( cubeObj );
            DontDestroyOnLoad ( cubeObjInstance );
        }

        //
        // Initialize the scene from "maps/secondary.map" bundle
        //
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene ( "Secondary", UnityEngine.SceneManagement.LoadSceneMode.Single );
        }
    }
}