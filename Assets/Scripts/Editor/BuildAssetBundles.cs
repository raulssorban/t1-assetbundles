﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class BuildAssetBundles
{
    [MenuItem ( "Build/AssetBundles" )]
    public static void DoBuild ()
    {
        var options = BuildAssetBundleOptions.UncompressedAssetBundle | BuildAssetBundleOptions.StrictMode;
        var platform = BuildTarget.StandaloneLinux64;

        if ( !Directory.Exists ( LoadAssetBundles.BundlesPath ) ) Directory.CreateDirectory ( LoadAssetBundles.BundlesPath );
        else
        {
            Directory.Delete ( LoadAssetBundles.BundlesPath, true );
            Directory.CreateDirectory ( LoadAssetBundles.BundlesPath );
        }

        Build ( LoadAssetBundles.BundlesPath, options, platform );
    }

    public static AssetBundleManifest Build ( string folder, BuildAssetBundleOptions options, BuildTarget platform )
    {
        return BuildPipeline.BuildAssetBundles ( folder, options, platform );
    }
}