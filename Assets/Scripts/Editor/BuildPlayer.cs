﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BuildPlayer
{
    [MenuItem ( "Build/Player" )]
    public static void DoBuild ()
    {
        var fileName = EditorUtility.SaveFilePanel ( "Building Player", Application.dataPath, Application.productName, "exe" );
        if ( string.IsNullOrEmpty ( fileName ) ) { Debug.Log ( "Building canceled." ); return; }

        var options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer;
        var target = BuildTarget.StandaloneWindows64;
        var build = Build ( fileName, options, target );

        Debug.Log ( build );
    }

    [MenuItem ( "Build/Package/Build (at path)" )]
    public static void DoBuildPackageAtPath ()
    {
        var fileName = EditorUtility.SaveFilePanel ( "Building Player", Application.dataPath, Application.productName, "exe" );
        if ( string.IsNullOrEmpty ( fileName ) ) { Debug.Log ( "Building canceled." ); return; }

        var fileDirectory = System.IO.Path.GetDirectoryName ( fileName );
        var assetBundlesDirectory = fileDirectory + "/Bundles";
        var target = BuildTarget.StandaloneWindows64;

        if(System.IO.Directory.Exists(fileDirectory) ) System.IO.Directory.Delete ( fileDirectory, true );
        System.IO.Directory.CreateDirectory ( fileDirectory );
        System.IO.Directory.CreateDirectory ( assetBundlesDirectory );

        BuildAssetBundles.Build ( assetBundlesDirectory, BuildAssetBundleOptions.UncompressedAssetBundle | BuildAssetBundleOptions.StrictMode, target );
        BuildPlayer.Build ( fileName, BuildOptions.Development | BuildOptions.ShowBuiltPlayer, target );
    }

    [MenuItem ( "Build/Package/Build" )]
    public static void DoBuildPackage ()
    {
        var fileName = Application.dataPath + "/../Build/" + Application.productName + ".exe";

        var fileDirectory = System.IO.Path.GetDirectoryName ( fileName );
        var assetBundlesDirectory = fileDirectory + "/Bundles";
        var target = BuildTarget.StandaloneWindows64;

        if ( System.IO.Directory.Exists ( fileDirectory ) ) System.IO.Directory.Delete ( fileDirectory, true );
        System.IO.Directory.CreateDirectory ( fileDirectory );
        System.IO.Directory.CreateDirectory ( assetBundlesDirectory );

        BuildAssetBundles.Build ( assetBundlesDirectory, BuildAssetBundleOptions.UncompressedAssetBundle | BuildAssetBundleOptions.StrictMode, target );
        BuildPlayer.Build ( fileName, BuildOptions.Development | BuildOptions.ShowBuiltPlayer, target );
    }

    public static string Build ( string name, BuildOptions options, BuildTarget target )
    {
        return BuildPipeline.BuildPlayer ( EditorBuildSettings.scenes, name, target, options );
    }
}