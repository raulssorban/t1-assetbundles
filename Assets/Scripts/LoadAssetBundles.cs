﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAssetBundles
{
    public Dictionary<string, AssetBundle> Bundles = new Dictionary<string, AssetBundle> ();
    public Dictionary<string, AssetBundle> Files = new Dictionary<string, AssetBundle> ();
    public AssetBundle LastLoadedBundle;
    public string Error;

    public static string BundlesPath
    {
        get { return Application.dataPath + "/../Bundles"; }
    }

    public IEnumerator LoadBundle ( string shortBundleName, string bundlePath, int version, bool loadAllAssetsAsync )
    {
        if ( Bundles.ContainsKey ( bundlePath ) )
        {
            Error = null;
            yield break;
        }

        if ( !File.Exists ( bundlePath ) )
        {
            Error = string.Format ( "Bundle doesn't exist at path - {0}", bundlePath );
            yield break;
        }

        var assetBundlePath = bundlePath;
        var assetBundleFullPath = new FileInfo ( assetBundlePath ).FullName;
        var assetBundleWww = WWW.LoadFromCacheOrDownload ( "file:///" + assetBundleFullPath, version );
        var assetbundle = new AssetBundle ();

        if ( assetBundleWww == null )
        {
            Error = string.Format ( "Couldn't load AssetBundle - {0}: {1}", shortBundleName, assetBundleWww.error );
            yield break;
        }

        yield return assetBundleWww;
        assetbundle = assetBundleWww.assetBundle;
        Bundles.Add ( shortBundleName, assetbundle );
        LastLoadedBundle = assetbundle;

        if ( assetbundle == null )
        {
            Error = string.Format ( "Couldn't load AssetBundle - {0}: AssetBundle is corrupted or manually edited and is not valid.", shortBundleName );
            yield break;
        }

        if ( loadAllAssetsAsync )
        {
            if ( !assetbundle.isStreamedSceneAssetBundle ) yield return assetbundle.LoadAllAssetsAsync ();
        }

        Error = null;
    }

    public T LoadAsset<T> ( string fileName, bool clear, bool withSubassets = false ) where T : UnityEngine.Object
    {
        var bundle = new AssetBundle ();
        if ( !Files.TryGetValue ( fileName, out bundle ) ) return null;

        var asset = bundle.LoadAsset<T> ( fileName );
        if ( asset == null ) return asset;

        if ( withSubassets ) bundle.LoadAssetWithSubAssets<T> ( fileName );

        if ( clear )
        {
            bundle.Unload ( false );
            UnityEngine.Object.DestroyImmediate ( bundle );
        }

        return asset;
    }
}